'use strict';
(function() {
  var app = angular.module('gemStore', ['store-directives', 'ngSails']);

    app.config(['$sailsProvider', function ($sailsProvider) {
        $sailsProvider.url = 'http://localhost:1337';
    }]);

  app.controller('StoreController', ['$sails', function($sails){
    var store = this;
    store.products = [];
    $sails.get('/product').success(function(data){
        store.products = data;
    });
  }]);

  app.controller('ReviewController',['$sails', function($sails) {
    this.review = {};

    this.addReview = function(product) {
        if(!product.reviews){
            product.reviews=[];
        }
        this.review.product_review = product.id;
        $sails.post('/review/' , this.review).success(function(data){
            product.reviews.push(data);
        });
      this.review = {};
    };
  }]);
})();